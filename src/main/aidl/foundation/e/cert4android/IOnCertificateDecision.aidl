package foundation.e.cert4android;

interface IOnCertificateDecision {

    void accept();
    void reject();

}
