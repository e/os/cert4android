package foundation.e.cert4android;

import foundation.e.cert4android.IOnCertificateDecision;

interface ICustomCertService {

    void checkTrusted(in byte[] cert, boolean interactive, boolean foreground, IOnCertificateDecision callback);
    void abortCheck(IOnCertificateDecision callback);

}
